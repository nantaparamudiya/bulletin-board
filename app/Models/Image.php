<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $guarded = [];

    public static $path = 'app/public/images';
    public static $dimensions = ['245', '300', '500'];
    public static $lang = ['en', 'jp', 'id']; 
    public $translatedAttributes = ['title', 'alt_desc', 'desc'];

    public function bulletin()
    {
        return $this->belongsTo('App\Models\Bulletin');
    }

    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery');
    }
}
