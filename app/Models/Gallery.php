<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = [];

    public function image()
    {
        return $this->hasOne('App\Models\Image');
    }
}
