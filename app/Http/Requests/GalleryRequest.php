<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'title'    => ['required'],
            'alt_desc' => ['nullable'],
            'desc'     => ['nullable'],
            'image'    => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:1024']
        ];

        if ($this->method() === 'put') {
            unset($rules);
            dd($rules);
        }

        dd($rules);

        return $rules;
    }
}
