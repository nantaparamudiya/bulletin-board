<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'title'    => ['required'],
            'alt_desc' => ['nullable'],
            'desc'     => ['nullable'],
            'image'    => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:1024']
        ];

        if ($this->method() === 'PUT') {
            $rules['locale'] = 'required';
            
            array_shift($rules['image']);
            array_unshift($rules['image'], 'nullable');
        }

        return $rules;
    }
}
