<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Models\Image;
use App\Models\ImageTranslation;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\ImageController;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        app()->setLocale($request->locale);

        $galleries = Gallery::orderBy('id', 'desc')->paginate(10);

        $render = function($image) { return ImageController::render($image); };

        return view('admin.gallery', compact('galleries', 'render'));
    }

    public function gallery(Request $request)
    {
        $this->checkLocale($request->locale);
  
        $galleries = Gallery::orderBy('id', 'desc')->paginate(10);
        $render = function($image) { return ImageController::render($image); };

        return view('gallery')->with(compact('galleries', 'render'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageRequest $request)
    {
        $data = $request->validated();

        $lang = array_flip(Image::$lang);

        $locale = (isset($lang[$request->locale])) ? $request->locale : 'en';

        if (! empty($data['image'])) {
            $imc = new ImageController();
            $image = $imc->upload($data['image']);
            unset($data['image']);
        } else {
            $image = ['name' => null]; 
        }

        $gallery = new Gallery();
        $gallery->save();

        $gallery->image()->create($image);

        $gallery->image->translateOrNew($locale)->title = $data['title'];
        $gallery->image->translateOrNew($locale)->alt_desc = $data['alt_desc'];
        $gallery->image->translateOrNew($locale)->desc = $data['desc'];
        $gallery->image->save();

        return \redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery, Request $request)
    {
        $this->checkLocale($request->locale, 'gallery.index');

        $gallery = Gallery::findOrFail($gallery->id);
        $image = $gallery->image->getTranslation($request->locale);

        return redirect()->back()->with([
            'modal' => '#editModal',
            'locale' => $request->locale,
            'gallery' => $gallery,
            'image' => $image
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(ImageRequest $request, Gallery $gallery)
    {
        $data = $request->validated();
        $image = $gallery->image;

        if (isset($data['image']) && !is_null($data['image'])) {
            $imc = new ImageController();

            $imc->delete($image->name, 'public/images');

            $img = $imc->upload($data['image']);
            unset($data['image']);
        } else {
            $img = $image->name;
        }

        $image->translateOrNew($data['locale'])->title = $data['title'];
        $image->translateOrNew($data['locale'])->alt_desc = $data['alt_desc'];
        $image->translateOrNew($data['locale'])->desc = $data['desc'];
        $image->save();
        
        $image->where('id', $image->id)->update([
            'name' => $img['name']
        ]);

        $message = 'Image Updated Successfully';

        return redirect()->route('gallery.index', $data['locale'])->with('success', $message);
    }

    public function confirmDelete(Gallery $gallery, Request $request)
    {
        $this->checkLocale($request->locale, 'gallery.index');

        $gallery = Gallery::findOrFail($gallery->id);
        $image = $gallery->image->getTranslation($request->locale);

        return redirect()->back()->with([
            'modal' => '#deleteModal',
            'locale' => $request->locale,
            'gallery' => $gallery,
            'image' => $image
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        $image = $gallery->image;

        $imc = new ImageController();
        $imc->delete($image->name, 'public/images');
        $gallery->where('id', $gallery->id)->delete();

        $message = 'Image Deleted Successfully';

        return redirect()->route('gallery.index', 'en')->with('success', $message);
    }

    private function checkLocale($locale, $route = 'gallery')
    {
        $lang = array_flip(Image::$lang);

        if (! isset($lang[$locale])) {
            return redirect(route($route, app()->getLocale()));
        }

        app()->setLocale($locale);
    }
}
