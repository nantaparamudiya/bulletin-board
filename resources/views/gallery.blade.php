@extends('layouts.app')

@section('title', 'Timedoor Challenge')

@section('header')
  <header>

    <nav class="navbar navbar-default mb-0" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <h2 class="font16 text-green mt-15"><b>Timedoor 30 Challenge Programmer</b></h2>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        @guest
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{{ route('home') }}">Home</a></li>
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
              <li class="dropdown">
                <a class="dropdown-toggle" id="lang" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  @switch (app()->getLocale())
                    @case('jp')
                      <img src="{{ asset('icons/jp.png') }}" alt="" width="20">
                    @break
                    @case('id')
                      <img src="{{ asset('icons/id.png') }}" alt="" width="20">
                    @break
                    @default
                      <img src="{{ asset('icons/uk.png') }}" alt="" width="20">
                  @endswitch
                </a>
                <div class="dropdown-menu" aria-labelledby="lang">
                  <a class="dropdown-item" href="{{ route('gallery.images', 'en') }}"><img src="{{ asset('icons/uk.png') }}" alt="" width="20"></a>
                  <a class="dropdown-item" href="{{ route('gallery.images', 'jp') }}"><img src="{{ asset('icons/jp.png') }}" alt="" width="20"></a>
                  <a class="dropdown-item" href="{{ route('gallery.images', 'id') }}"><img src="{{ asset('icons/id.png') }}" alt="" width="20"></a>
                </div>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        @else
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{{ route('home') }}">Home</a></li>
              <li>
                <form action="{{ route('logout') }}" method="post">
                  @csrf
                  <button type="submit" class="logout">Logout</button>
                </form>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        @endguest
      </div><!-- /.container-fluid -->
    </nav>

    @if ($errors->any())
      <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
          {{ $error }}
          <br>
        @endforeach
      </div>
    @elseif (! is_null(session('success')))
      <div class="alert alert-success">
        {{ session('success') }}
      </div>
    @endif

  </header>
@endsection

@section('content')
<main>
    <div class="section bg-white">
      <div class="container">
        <div class="text-center">
            <h1 class="text-green mb-30"><b>Gallery</b></h1>
        </div>
        @foreach($galleries as $gallery)
          <img src="{{ $render($gallery->image->name) }}" alt="{{ $gallery->image->alt_desc }}" class="img-thumbnail" data-toggle="modal" data-target="#infoModal" onclick="showDetail(this)" data-title="{{ $gallery->image->title }}" data-desc="{{ $gallery->image->desc }}">
        @endforeach
      </div>
    </div>
  </main>

  <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
          </button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">
          <img class="img-fluid" id="img-info" src="" alt="">
          <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" readonly>
          </div>
          <div class="form-group">
            <label for="alt">Alt Description</label>
            <input type="text" class="form-control" id="alt" readonly>
          </div>
          <div class="form-group">
            <label for="desc">Description</label>
            <textarea class="form-control" id="desc" readonly></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  function showDetail(e)
  {
    const title = e.dataset.title;
    const alt = e.alt;
    const desc = e.dataset.desc;
    const src = e.src;

    document.querySelector('#myModalLabel').innerHTML = title;
    document.querySelector('#img-info').src = src;
    document.querySelector('#img-info').alt = alt;
    document.querySelector('#title').value = title;
    document.querySelector('#alt').value = alt;
    document.querySelector('#desc').value = desc;
  }
</script>
@endsection

@section('footer')
  <footer>

    <p class="font12">
      Copyright &copy; {{ date('Y') }} by <a href="https://timedoor.net" class="text-green">PT. TIMEDOOR INDONESIA</a>
    </p>
    
  </footer>
@endsection