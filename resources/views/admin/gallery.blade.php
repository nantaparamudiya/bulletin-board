@extends('layouts.admin')

@section('title', 'Timedoor Admin | Gallery')

@section('content')
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>T</b>D</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Timedoor</b> Admin</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="hidden-xs">Hello, Admin </span>
                </a>
                <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                    <p>
                    Administrator
                    </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="text-right">
                    <a href="login.php" class="btn btn-danger btn-flat">Sign out</a>
                    </div>
                </li>
                </ul>
            </li>
            </ul>
        </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        @include('admin.components.sidebar')
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Gallery
        </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        {{ $error }}
                        <br>
                        @endforeach
                    </div>
                @elseif (session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h1 class="font-18 m-0">Timedoor Challenge</h1>
                    </div>
                    <div class="box-body">
                        <div class="bordered-box mb-20">
                            <form action="{{ route('gallery.store') }}" method="post" enctype="multipart/form-data">
                            @csrf

                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="{{ route('gallery.index', 'en') }}"><img src="{{ asset('icons/uk.png') }}" width="15"> en</a></li>
                                <li><a href="{{ route('gallery.index', 'jp') }}"><img src="{{ asset('icons/jp.png') }}" width="15"> jp</a></li>
                                <li><a href="{{ route('gallery.index', 'id') }}"><img src="{{ asset('icons/id.png') }}" width="15"> id</a></li>
                            </ul>

                            <input type="hidden" name="locale" value="{{ app()->getLocale() }}">

                            <table class="table table-no-border mb-0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label>Choose image from your computer :</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-default btn-file">
                                                            <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image">
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <button type="button" class="btn btn-primary form-control" data-toggle="modal" data-target="#inputModal">Next</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>
                                    <th>Alt Image</th>
                                    <th>Desc Image</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($galleries as $gallery)
                                    <tr>
                                        <td>{{ $i++ . '.' }}</td>
                                        <td>{{ $gallery->image->title }}</td>
                                        <td>{{ $gallery->image->alt_desc }}</td>
                                        <td>{{ $gallery->image->desc }}</td>
                                        <td><img src="{{ $render($gallery->image->name) }}" alt="{{ $gallery->image->alt_desc }}" width="100"></td>
                                        <td>
                                            <a href="{{ route('gallery.edit', [$gallery->id, app()->getLocale()]) }}"><i class="fa fa-pencil"></i> Edit</a>
                                            <br>
                                            <a class="text-danger" href="{{ route('gallery.delete', [$gallery->id, app()->getLocale()]) }}"><i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.1.0
        </div>
        <strong>Copyright &copy; {{ date('Y') }} <a href="https://timedoor.net" class="text-green">Timedoor Indonesia</a>.</strong> All rights reserved.
    </footer>
</div>

@include('admin.components.modals')
@endsection