<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body">
          @csrf

          <table class="table table-no-border mb-0">
            <tbody>
              <tr>
                <td width="150">
                  <b>Title</b>
                </td>
                <td>
                  <div class="form-group mb-0">
                    <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>Alt Description</b>
                </td>
                <td>
                  <div class="form-group mb-0">
                    <input type="text" name="alt_desc" class="form-control" value="{{ old('alt_desc') }}">
                  </div>
                </td>
              </tr>
              <tr>
                <td><b>Image Description</b>
                </td>
                <td>
                  <div class="form-group mb-0">
                    <input type="text" name="desc" class="form-control" value="{{ old('desc') }}">
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>

    </div>
  </div>
</div>

@if (session()->has('modal'))
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="{{ route('gallery.update', session('gallery.id')) }}" method="post" enctype="multipart/form-data">
    <div class="modal-dialog">
      <div class="modal-content">
        
        <div class="modal-body">
            @csrf
            @method('put')

            <input type="hidden" name="locale" value="{{ session('locale') }}">

            <ul class="nav navbar-nav navbar-right">
              <li><a href="{{ route('gallery.edit', [session('gallery.id'), 'en']) }}"><img src="{{ asset('icons/uk.png') }}" width="15"> en</a></li>
              <li><a href="{{ route('gallery.edit', [session('gallery.id'), 'jp']) }}"><img src="{{ asset('icons/jp.png') }}" width="15"> jp</a></li>
              <li><a href="{{ route('gallery.edit', [session('gallery.id'), 'id']) }}"><img src="{{ asset('icons/id.png') }}" width="15"> id</a></li>
            </ul>

            <table class="table table-no-border mb-0">
              <tbody>
                <tr>
                  <td width="150">
                    <strong>Title ({{ session('locale') }})</strong>
                    <br>
                    <div class="form-group mb-0">
                      <input type="text" name="title" class="form-control" value="{{ session('image.title') ?? old('title') }}">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Alt Description ({{ session('locale') }})</strong>
                    <br>
                    <div class="form-group mb-0">
                      <input type="text" name="alt_desc" class="form-control" value="{{ session('image.alt_desc') ?? old('alt_desc') }}">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Image Description ({{ session('locale') }})</strong>
                    <br>
                    <div class="form-group mb-0">
                      <input type="text" name="desc" class="form-control" value="{{ session('image.desc') ?? old('desc') }}">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img src="{{ $render(session('gallery.image.name')) }}" alt="{{ session('image.alt_desc') }}" width="100">
                    <br>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image">
                                </span>
                            </span>
                        </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>

      </div>
    </div>
  </form>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Delete Image</h4>
        </div>
      <div class="modal-body">
        <p class="text">Are you sure want to delete this <strong>Image</strong>?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('gallery.destroy', session('gallery.id')) }}" method="post">
          @csrf
          @method('DELETE')
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
        </form>
      </div>
    </div>
  </div>
</div>
@endif